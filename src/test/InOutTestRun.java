package test;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.Test;

import generic.BaseTestIO;
import generic.ExcelIO;
import pomIO.RegisterIO;
import pomIO.LoginPageIO;
import pomIO.LogsIO;
import pomIO.ReportIO;
import pomIO.SettingsIO;
import pomIO.StoreIO;
import pomIO.SubscriptionIO;

public class InOutTestRun extends BaseTestIO
{
	static int row=0;
	static int coulumn=0;
	static String enumber=ExcelIO.getCellData(INPUT_PATH, "Credentials", 1, 0);
	static String DD=ExcelIO.getCellData(INPUT_PATH, "Credentials", 1, 1);
	static String MM=ExcelIO.getCellData(INPUT_PATH, "Credentials", 1, 2);
	static String YYYY=ExcelIO.getCellData(INPUT_PATH, "Credentials", 1, 3);
	static String A_Enumber=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 1, 0);
	static String Reg_number=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 1, 1);
	static String EmpType=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 1, 2);
	static String Name=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 1, 3);
	static String Gender=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 1, 4);
	static String Location=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 1, 5);
	static String Role=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 1, 6);
	static String Designation=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 1, 7);
	static String Department=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 1, 8);
	static String DOB=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 1, 9);
	static String Store=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 1, 10);
	
	//StoreTAB
	static String StoreNumber=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 22, 0);
	static String StoreName=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 22, 1);
	static String StoreAddress=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 22, 2);
	static String StoreRadius=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 22, 3);
	static String StoreLatitude=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 22, 4);
	static String StoreLongitude=ExcelIO.getCellData(INPUT_PATH, "AddEmployee", 22, 5);
	
	
//	@Test(priority=1)
//	public void Register() throws InterruptedException
//	{
//		RegisterIO r=new RegisterIO(driver);
//		
//		Reporter.log("Scenario : Creating new domain through Novaders website",true);
//		SubscriptionIO sub=new SubscriptionIO(driver);
//		sub.createDomain(); //Freemium
//		Reporter.log("\nScenario : Check elements present is Freemium registration page:",true);
//		sub.verifyNameReg();
//		sub.verifyPwdReg();
//		sub.verifyCPwdReg();
//		sub.verifyusernameReg();
//		sub.verifyOrgReg();
//		sub.verifyDomainReg();
//		sub.verifyverifySubmitReg();
//		
//		Reporter.log("\nDomain Creation",true);
//		Reporter.log("\nScenario : Create already existing domain",true);
//		
//		sub.entername("Nuthan");
//		sub.enterPassword("1111");
//		sub.enterCnPassword("1111");
//		sub.enterUsername("nuthan94@gmail.com");
//		sub.enterOrganization("abc");
//		sub.clickRegDomain();
//		sub.verifyErrorMSG();
//	
//		Reporter.log("\nScenario : Create new Domain",true);
//		sub.entername("dm1");
//		sub.enterPassword("1111");
//		sub.enterCnPassword("1111");
//		sub.enterUsername("nuthan94@gmail.com");
//		sub.enterOrganization("dm1");
//		sub.clickRegDomain();
//		sub.confirmMessage();
//		
//		Reporter.log("\nScenario : Activate domain account ",true);	
//		Thread.sleep(30000);
//	
		//Opening gmail account
//		driver.get("https://www.google.com/gmail/");
//		Thread.sleep(10000);
//		driver.findElement(By.xpath("//p[.='nuthan94@gmail.com']")).click();
//		Thread.sleep(5000);
//		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("*********");
//		Thread.sleep(3000);
//		driver.findElement(By.xpath("//*[@id='passwordNext']/content")).submit();
//		Thread.sleep(3000);
//		driver.findElement(By.xpath("//*[@id='gbqfq']")).sendKeys("coffeeDay");
//		Thread.sleep(10000);
//		driver.findElement(By.xpath("//button[@aria-label='Search Gmail']")).submit();
//		Thread.sleep(4000);
//		driver.findElement(By.xpath("//*[@id=':1lu']/td[5]")).click();
//		Thread.sleep(5000);
//		driver.findElement(By.xpath("//u[.='Activate your account']/..")).click();
//		Thread.sleep(150000);
//		
//		Reporter.log("Scenario : The elements present in InOut register page are:",true);
//		r.verifyInoutLogo();
//		r.verifyUserLogin();
//		r.verifyAdminLogin();
//		r.verifyAppStore();
//		r.verifyGooglePlay();
//		
//		Reporter.log("\nScenario : The elements present in User Login page are:",true);
//		r.verifyEmpnumber();
//		r.verifyDD();
//		r.verifyMM();
//		r.verifyYYYY();
//		r.verifyUserLoginBTN();
//		r.verifyUserFP();
//		r.verifySignUP();
//		
//		Reporter.log("\nScenario : The elements present in Admin login are:",true);
//		r.clickAdminLogin();
//		r.verifyUsername();
//		r.verifyPassword();
//		r.verifyAdminLoginBTN();
//		r.verifyAdminFP();
//		
//		Reporter.log("\nScenario : Domain creation",true);
//		r.clickRegister();
//		Reporter.log("\nScenario : Check the types of Subscription plans:",true);
//		SubscriptionIO s=new SubscriptionIO(driver);
//		s.verifyFreemium();
//		s.verifyPremium();
//		s.verifyEnterprise();
//		
//		Reporter.log("\nScenario : Choose Freemium",true);
//		s.chooseFreemium();
//		s.FreemiumMSG();
//		
//		Reporter.log("\nScenario : Check elements present is Freemium registration page:",true);
//		s.verifyNameReg();
//		s.verifyPwdReg();
//		s.verifyCPwdReg();
//		s.verifyusernameReg();
//		s.verifyOrgReg();
//		s.verifyDomainReg();
//		s.verifyverifySubmitReg();
//		
//		Reporter.log("\nDomain Creation",true);
//		Reporter.log("\nScenario : Create already existing domain",true);
//		
//		s.entername("Cupcake");
//		s.enterPassword("1111");
//		s.enterCnPassword("1111");
//		s.enterUsername("nuthan94@gmail.com");
//		s.enterOrganization("hilli");
//		s.clickRegDomain();
//		s.verifyErrorMSG();
//	
//		Reporter.log("\nScenario : Create new Domain",true);
//		s.entername("Cupcake");
//		s.enterPassword("1111");
//		s.enterCnPassword("1111");
//		s.enterUsername("nuthan94@gmail.com");
//		s.enterOrganization("coffeeDay");
//		s.clickRegDomain();
//		s.confirmMessage();
//		
//		Reporter.log("\nScenario : Activate domain account ",true);	
//		//Opening gmail account
//		driver.get("https://www.google.com/gmail/");
//		Thread.sleep(10000);
//		driver.findElement(By.xpath("//p[.='nuthan94@gmail.com']")).click();
//		Thread.sleep(5000);
//		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("");
//		Thread.sleep(3000);
//		driver.findElement(By.xpath("//*[@id='passwordNext']/content")).submit();
//		Thread.sleep(3000);
//		driver.findElement(By.xpath("//*[@id='gbqfq']")).sendKeys("coffeeDay");
//		Thread.sleep(10000);
//		driver.findElement(By.xpath("//button[@aria-label='Search Gmail']")).submit();
//		Thread.sleep(4000);
//		driver.findElement(By.xpath("//*[@id=':1lu']/td[5]")).click();
//		Thread.sleep(5000);
//		driver.findElement(By.xpath("//u[.='Activate your account']/..")).click();
//		Thread.sleep(150000);
//
//		
//	}

	
	@Test(priority=1)
	public void testLogin()
	{
		RegisterIO reg=new RegisterIO(driver);
		Reporter.log("Scenario : The elements present in InOut register page are:",true);
		reg.verifyInoutLogo();
		reg.verifyUserLogin();
		reg.verifyAdminLogin();
		reg.verifyAppStore();
		reg.verifyGooglePlay();
		
		Reporter.log("\nScenario : The elements present in User Login page are:",true);
		reg.verifyEmpnumber();
		reg.verifyDD();
		reg.verifyMM();
		reg.verifyYYYY();
		reg.verifyUserLoginBTN();
		reg.verifyUserFP();
		reg.verifySignUP();
		
		Reporter.log("\nScenario : The elements present in Admin login are:",true);
		reg.clickAdminLogin();
		reg.verifyUsername();
		reg.verifyPassword();
		reg.verifyAdminLoginBTN();
		reg.verifyAdminFP();
		
		
		
	}
//	@Test(priority=1)
//	public void testLogin() throws InterruptedException, IOException
//	{	
//		Reporter.log("Scenario 1: Elements present in Login page are: ",true);	
//		LoginPageIO l=new LoginPageIO(driver);
//		l.verifyenumber();
//		l.verifyDD();
//		l.verifyMM();
//		l.verifyYYYY();
//		l.verifyLoginButton();	
//	
//		Reporter.log("\nScenario 2: Enter invalid Employee number",true);
//		l.setEnumber("invalid");
//		l.setDate("12");
//		l.setMonth("02");
//		l.setYear("1990");
//		l.clickSubmit();
//		Thread.sleep(2000);
//		
//		TakesScreenshot t=(TakesScreenshot) driver;
//		File srcFile=t.getScreenshotAs(OutputType.FILE);
//		File destFile=new File("./C_screenshots/invalid_login_credentials.png");
//		FileUtils.copyFile(srcFile, destFile);
//		Reporter.log("Screenshot taken for invalid login credentials",true);
//		
//		driver.navigate().refresh();
//
//		Reporter.log("\nScenario 3: Enter INVALID DATE FORMAT with VALID EMPLOYEE NUMBER",true);
//		l.setEnumber(enumber);
//		l.setDate("99");
//		l.setMonth("09");
//		l.setYear("9999");
//		l.clickSubmit();
//		
//		TakesScreenshot t1=(TakesScreenshot) driver;
//		File srcFile1=t1.getScreenshotAs(OutputType.FILE);
//		File destFile1=new File("./C_screenshots/invalid_date_format.png");
//		FileUtils.copyFile(srcFile1, destFile1);
//		Reporter.log("Screenshot taken for invalid date format",true);
//		
//		Reporter.log("\nScenario 4: Enter invalid DOB/Password with valid Employee number",true);
//		l.setEnumber(enumber);
//		l.setDate("01");
//		l.setMonth("02");
//		l.setYear("1997");
//		l.clickSubmit();
//		
//		TakesScreenshot t2=(TakesScreenshot) driver;
//		File srcFile2=t2.getScreenshotAs(OutputType.FILE);
//		File destFile2=new File("./C_screenshots/Invalid_DOB.png");
//		FileUtils.copyFile(srcFile2, destFile2);
//		Reporter.log("Screenshot taken for Invalid DOB",true);
//		
//		Reporter.log("\nScenario 5: Login as Deleted user",true);
//		l.setEnumber("T8089310000");
//		l.setDate("01");
//		l.setMonth("11");
//		l.setYear("2017");
//		l.clickSubmit();
//		TakesScreenshot t3=(TakesScreenshot) driver;
//		File srcFile3=t3.getScreenshotAs(OutputType.FILE);
//		File destFile3=new File("./C_screenshots/Deleted_user_login.png");
//		FileUtils.copyFile(srcFile3, destFile3);
//		Reporter.log("Screenshot taken for: Deleted user login",true);
//		
//		Reporter.log("\nScenario 6: Login as User",true);
//		l.setEnumber("0002");
//		l.setDate("03");
//		l.setMonth("10");
//		l.setYear("2017");
//		l.clickSubmit();
//		TakesScreenshot t4=(TakesScreenshot) driver;
//		File srcFile4=t4.getScreenshotAs(OutputType.FILE);
//		File destFile4=new File("./C_screenshots/User_No_access.png");
//		FileUtils.copyFile(srcFile4, destFile4);
//		Reporter.log("Screenshot taken for: No access for User",true);
//				
//		Reporter.log("\nScenario 7: Leave blank for Employee number, DOB and click on Login button",true);
//		l.blank();
//	
//		Reporter.log("\nScenario 8: Enter valid Employee number",true);
//		l.setEnumber(enumber);
//		l.setDate(DD);
//		l.setMonth(MM);
//		l.setYear(YYYY);
//		l.clickSubmit();
//}
//	
//	@Test(priority=2)
//	public void testHomePage()
//	{
//		LoginPageIO l=new LoginPageIO(driver);
//		ReportIO r=new ReportIO(driver);
//		MasterIO m=new MasterIO(driver);
//		SettingsIO s=new SettingsIO(driver);
//		LogsIO lo=new LogsIO(driver);
////		l.setEnumber(enumber);
////		l.setDate(DD);
////		l.setMonth(MM);
////		l.setYear(YYYY);
////		l.clickSubmit();
//		
//		Reporter.log("\nScenario 9: When logged in with valid credential, Report page should display",true);
//		Reporter.log("The selected tab is:",true);
//		r.verifyReport();
//				
//		Reporter.log("\nScenario 10: The tabs present in Homepage are: ",true);
//		r.verifyReport();
//		m.verifyMaster();
//		s.verifySettings();
//		lo.verifyLogs();	
//	}
//	
//	@Test(priority=3)
//	public void testReport() throws InterruptedException
//	{
//		LoginPageIO l=new LoginPageIO(driver);
//		l.setEnumber(enumber);
//		l.setDate(DD);
//		l.setMonth(MM);
//		l.setYear(YYYY);
//		l.clickSubmit();
//		ReportIO r=new ReportIO(driver);
//		r.clickReport();
//		
//		Reporter.log("\nScenario 11: The elements in Report page are:",true);
//		r.verifyToday();
//		r.verifyYesterday();
//		r.verifyLast7Days();
//		r.verifyLast30Days();
//		r.verifyShowAll();
//		r.verifyFilter();
//		r.verifyTakeATour();
//		r.verifyEmployeeSearch();
//		
//		Reporter.log("\nScenario 12: Click on Today",true);	
//		Thread.sleep(5000);
//		r.clickToday();
//		Thread.sleep(5000);
//		Reporter.log("Displays the attendance of Today i.e present day",true);
//		
//		Reporter.log("\nScenario 13: Click on Yesterday",true);	
//		r.clickYesterday();
//		Thread.sleep(5000);
//		Reporter.log("Displays the attendance of Yesterday i.e previous day",true);
//		
//		Reporter.log("\nScenario 14: Click on Last 7 Days",true);	
//		r.click7Days();
//		Thread.sleep(5000);
//		Reporter.log("Displays the attendance of Last 7 days",true);
//		
//		Reporter.log("\nScenario 15: Click on Last 30 days",true);	
//		r.clickMonth();
//		Thread.sleep(5000);
//		Reporter.log("Displays the attendance of 1 month i.e last 30 days",true);
//		
//		Reporter.log("\nScenario 16: Expand button to check the attendance of employee",true);
//		r.clickEmpExpand();
//		r.clickExpData();
//	}
//	
//	@Test(priority=4)
//	public void testSettings() throws InterruptedException
//	{
//		LoginPageIO l=new LoginPageIO(driver);
//		SettingsIO s=new SettingsIO(driver);
//		
//		l.setEnumber(enumber);
//		l.setDate(DD);
//		l.setMonth(MM);
//		l.setYear(YYYY);
//		l.clickSubmit();	
//		
//		Reporter.log("\nGeofence radius:",true);
//		
//		Reporter.log("\nThe elements present in Settings are:",true);
//		s.clickSettings();
//		s.verifyOrganisationSettings();
//		s.verifyPreferences();
//		s.verifyExport();
//		s.verifyImport();
//
//		s.clickOrganisationSettings();
//		
//		Reporter.log("\nScenario : Clear the radius value. Leave Radius value blank and click on Submit",true);
//		s.radiusBlank();
//		
//		Reporter.log("\nScenario : Enter alphabets to Radius",true);
//		s.setRadius("Alphabets");
//		
//		Reporter.log("\nScenario : Enter Special characters to Radius",true);
//		s.setRadius("!@#$%^&*");
//		
//		Reporter.log("\nScenario : Enter numbers",true);
//		s.setRadius("20000");	
//		
//		Reporter.log("\nScenario : Login as different user. Radius value should be same",true);
//		s.clickLogout();
//		s.clickUserlogout(enumber);
//		
//		Reporter.log("\nLogin as NEP0012",true);
//		l.setEnumber("NEP0012");
//		l.setDate("01");
//		l.setMonth("05");
//		l.setYear("1993");
//		l.clickSubmit();	
//		
//		s.clickSettings();
//		s.verifyRadius();
//	}
//	
//	
//	@Test(priority=5)
//	public void testAddEmployee() throws InterruptedException, IOException
//	{
//		
//		LoginPageIO l=new LoginPageIO(driver);
//		SettingsIO s=new SettingsIO(driver);
//		
//		l.setEnumber(enumber);
//		l.setDate(DD);
//		l.setMonth(MM);
//		l.setYear(YYYY);
//		l.clickSubmit();
//		
//		Reporter.log("\nScenario : Creating new Employee "+A_Enumber,true);
//		MasterIO m=new MasterIO(driver);
//		m.clickMasterData();
//	//	m.empCount();
//		m.clickAddEmployee();
//		m.setEnumber(A_Enumber);
//		m.setRegNumber(Reg_number);
//		m.selectEmployeeType(EmpType);	//Permanent-Temporary
//		m.setEname(Name);
//		m.selectGender(Gender);
//		m.selectLocation(Location);
//		m.selectRole(Role);
//		m.selectDesignation(Designation);
//		m.selectDepartment(Department);
//		m.setDOB(DOB);
//		m.setStore(Store);
//		m.clickSubmit();
//	//	m.clickEmpCreationOK();
//		
//		Reporter.log(A_Enumber+" created successfully\n",true);
//		Thread.sleep(5000);
//		TakesScreenshot t=(TakesScreenshot) driver;
//		File srcFile=t.getScreenshotAs(OutputType.FILE);
//		File destFile=new File("./C_screenshots/Employee_creation.png");
//		FileUtils.copyFile(srcFile, destFile);
//		Reporter.log("Screenshot taken Newly added employee\n",true);
//		m.clickEmpCreationOK();
//		
//		Reporter.log("Scenario : Search for employee number "+A_Enumber+" whether its created or not",true);		
//		
//		m.clickMasterData();
//		m.clickEmployeeSearch(A_Enumber);
//		TakesScreenshot t1=(TakesScreenshot) driver;
//		File srcFile1=t1.getScreenshotAs(OutputType.FILE);
//		File destFile1=new File("./C_screenshots/Search_Employee_created.png");
//		FileUtils.copyFile(srcFile1, destFile1);
//		Reporter.log("Screenshot taken to confirm New employee is created\n",true);
//		m.clickEmpSearchCancel();
//	
//		Reporter.log("\nScenario : Create using existing Employee data "+A_Enumber,true);
//		MasterIO m1=new MasterIO(driver);
//		m1.clickMasterData();
//		m1.clickAddEmployee();
//		m1.setEnumber(A_Enumber);
//		m1.setRegNumber(Reg_number);
//		m1.selectEmployeeType(EmpType);	//Permanent-Temporary
//		m1.setEname(Name);
//		m1.selectGender(Gender);
//		m1.selectLocation(Location);
//		m1.selectRole(Role);
//		m1.selectDesignation(Designation);
//		m1.selectDepartment(Department);
//		m1.setDOB(DOB);
//		m1.clickSubmit();
//		m1.verifyErrorMessage();
//		
//	}
//
//
//	@Test(priority=6)
//	public void testEditEmployee() throws InterruptedException, IOException
//	{
//		LoginPageIO l=new LoginPageIO(driver);
//		SettingsIO s=new SettingsIO(driver);
//		
//		l.setEnumber(enumber);
//		l.setDate(DD);
//		l.setMonth(MM);
//		l.setYear(YYYY);
//		l.clickSubmit();
//		
//		Reporter.log("Scenario  : Edit employee",true);
//		MasterIO m=new MasterIO(driver);
//		m.clickMasterData();
//		m.clickEditEmpsearch(A_Enumber);
//		m.clickEditBTN();
//		m.changeName("Chinmay");
//		m.changeDOB("20/01/1995");
//		m.addStore("Novaders");
//		driver.navigate().refresh();
//		
//		m.clickMasterData();
//		m.clickEmpsearch(enumber);
//		m.clickEditSearchBTN();
//		m.clickEditBTN();
//		Reporter.log("Screenshot taken\nChanges:1. "+Name+" changed to Chinmay.\n\tDOB "+DOB+" changed to 20/01/1995\n\tNovaders store added",true);
//		TakesScreenshot t=(TakesScreenshot) driver;
//		File srcFile=t.getScreenshotAs(OutputType.FILE);
//		File destFile=new File("./C_screenshots/EmployeeEdit.png");
//		FileUtils.copyFile(srcFile, destFile);
//	}
//	
//	@Test(priority=7)
//	public void testEmpDelete() throws InterruptedException, IOException
//	{
//		Reporter.log("Delete employee: Emp number- "+A_Enumber,true);
//		MasterIO m=new MasterIO(driver);
//		m.clickMasterData();
//		m.clickEmpsearch(A_Enumber);
//		m.clickEditSearchBTN();
//		m.clickDelCheckbox();
//		m.clickDelete();
//		m.clickNo();
//		Reporter.log("Click on NO. User should not delete",true);
//		m.clickDelete();
//		m.clickYes();
//		m.clickDelOK();
//		Reporter.log("Searching for "+enumber,true);
//		m.clickEmpsearch(A_Enumber);
//		m.clickEditSearchBTN();
//		TakesScreenshot t=(TakesScreenshot) driver;
//		File srcFile=t.getScreenshotAs(OutputType.FILE);
//		File destFile=new File("./C_DeleteEmployee.png");
//		FileUtils.copyFile(srcFile, destFile);
//		
//	}
//	
//	@Test(priority=8)
//	public void testStore()
//	{
//		StoreIO s=new StoreIO(driver);
//		s.clickStoreTab();
//		s.clickAddStore();
//		s.enterStoreNumber(StoreNumber);
//		s.enterStoreName(StoreName);
//		s.enterStoreAddress(StoreAddress);
//		s.enterStoreRadius(StoreRadius);
//		s.enterStoreLatitude(StoreLatitude);
//		s.enterStoreLongitude(StoreLongitude);
//		s.clickStoreSubmit();
//	}

}