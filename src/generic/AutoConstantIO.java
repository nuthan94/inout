package generic;

public interface AutoConstantIO 
{
		String CHROME_KEY="webdriver.chrome.driver";
		String CHROME_VALUE="./driverIO/chromedriver.exe";
//		String GECKO_KEY="webdriver.gecko.driver";
//		String GECKO_VALUE="./driverIO/geckodriver.exe";
		String INPUT_PATH="./dataIO/input.xlsx";
		String IMPORT_PATH="./dataIO/import.csv";
	}