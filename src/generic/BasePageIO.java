package generic;

	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.WebDriverWait;
	import org.testng.Reporter;

	public class BasePageIO 
	{
		public WebDriver driver;
		public BasePageIO(WebDriver driver)
		{
			this.driver=driver;
		}
		public void verifyElementPresent(WebElement element)
		{
			WebDriverWait wait=new WebDriverWait(driver, 5);
			try{
				wait.until(ExpectedConditions.visibilityOf(element));
			}
			catch(Exception e)
			{
				String text1=element.getText();
				Reporter.log(text1,true);
			}
		}
	}