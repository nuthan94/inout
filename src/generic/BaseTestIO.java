package generic;

	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.testng.annotations.BeforeSuite;

	public abstract class BaseTestIO implements AutoConstantIO
	{
		public WebDriver driver;
		@BeforeSuite
		public void openApplication() throws InterruptedException
		{
		//	System.setProperty(GECKO_KEY, GECKO_VALUE);
			System.setProperty(CHROME_KEY, CHROME_VALUE);
			
			driver=new ChromeDriver();
			driver.get("https://testing.inoutapps.com");
//			driver.manage().window().maximize();
		//	driver.get("https://cardinal-newui.2docstore.com/#/report");
		//	Thread.sleep(10000);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);		
		}
		
//		@AfterSuite
//		public void closeApplication()
//		{
//			driver.quit();
//		}


	}