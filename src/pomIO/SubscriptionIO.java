package pomIO;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import generic.BasePageIO;

public class SubscriptionIO extends BasePageIO{

	public SubscriptionIO(WebDriver driver) 
	{
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//div[.='Freemium']")
	private WebElement Freemium;
	
	@FindBy(xpath="//div[.='Premium Package']")
	private WebElement Premium;
	
	@FindBy(xpath="//div[.='Enterprise']")
	private WebElement Enterprise;
	
	@FindBy(xpath="(//button[.='Choose Plan'])[1]")
	private WebElement FreemiumPlan;
	
	@FindBy(xpath="(//button[.='Choose Plan'])[2]")
	private WebElement PremiumPlan;
	
	@FindBy(xpath="//*[@id='wrapper']/div/div[1]/h4")
	private WebElement FreemiumMSG;
	
	//Registration page
	
	@FindBy(xpath="//*[@id='loginForm']/div[1]/div/input")
	private WebElement name;
	
	@FindBy(xpath="//*[@id='loginForm']/div[2]/div/input")
	private WebElement password;
	
	@FindBy(xpath="//*[@id='loginForm']/div[3]/div/input")
	private WebElement confirmPassword;
	
	@FindBy(xpath="//*[@id='loginForm']/div[4]/div/input")
	private WebElement username;
	
	@FindBy(xpath="//*[@id='loginForm']/div[5]/div/input")
	private WebElement organization;
	
	@FindBy(xpath="//*[@id='loginForm']/div[6]/div/div[1]/div/input")
	private WebElement domain;
	
	@FindBy(xpath="//*[@id='loginForm']/button")
	private WebElement submit;
	
	//Activation mail message
	
	@FindBy(xpath="//*[@id='wrapper']/div/div/div/h1")
	private WebElement activationMSG;
	
	@FindBy(xpath="//span[.='URL is already exists']")
	private WebElement URLexistMSG;
//	
//	@FindBy(xpath="")
//	private WebElement ;
//	
//	@FindBy(xpath="")
//	private WebElement ;
//	
//	@FindBy(xpath="")
//	private WebElement ;
//	
//	@FindBy(xpath="")
//	private WebElement ;
//	
//	@FindBy(xpath="")
//	private WebElement ;
//	
//	@FindBy(xpath="")
//	private WebElement ;
//	
//	@FindBy(xpath="")
//	private WebElement ;
	
	WebDriverWait wait=new WebDriverWait(driver, 10);

	public void verifyFreemium() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Freemium));
		Reporter.log(Freemium.getText(),true);
		}
		catch(Exception e){
			Reporter.log("Freemium is not displayed",true);
			Assert.fail();
		}
	}

	public void verifyPremium()
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Premium));
		Reporter.log(Premium.getText(),true);	
	}
		catch(Exception e){
		Reporter.log("Freemium is not displayed",true);
		Assert.fail();
	}
	}
		

	public void verifyEnterprise() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(Enterprise));
		Reporter.log(Enterprise.getText(),true);	
		}
		catch(Exception e){
			Reporter.log("Freemium is not displayed",true);
			Assert.fail();
		}
	}

	public void FreemiumMSG() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(FreemiumMSG));
		Reporter.log(FreemiumMSG.getText(),true);		
		}
		catch(Exception e){
			Reporter.log("FreemiumMSG is not displayed",true);
			Assert.fail();
		}
	}

	public void chooseFreemium() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(FreemiumPlan));
		FreemiumPlan.click();	
		}
		catch(Exception e){
			Reporter.log("FreemiumPlan is not displayed",true);
			Assert.fail();
		}
		
	}

	public void verifyNameReg() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(name));
			Reporter.log("Name",true);
		}
		catch(Exception e)
		{
			Reporter.log("Name is not displaying",true);
			Assert.fail();
		}
	}

	public void verifyPwdReg() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(password));
			Reporter.log("Password",true);
		}
		catch(Exception e)
		{
			Reporter.log("Password is not displaying",true);
			Assert.fail();
		}	
	}

	public void verifyCPwdReg() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(confirmPassword));
			Reporter.log("Confirm Password",true);
		}
		catch(Exception e)
		{
			Reporter.log("Confirm Password is not displaying",true);
			Assert.fail();
		}
	}

	public void verifyusernameReg() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(username));
			Reporter.log("username",true);
		}
		catch(Exception e)
		{
			Reporter.log("username is not displaying",true);
			Assert.fail();
		}
		
	}

	public void verifyOrgReg()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(organization));
			Reporter.log("organization",true);
		}
		catch(Exception e)
		{
			Reporter.log("organization is not displaying",true);
			Assert.fail();
		}
		
	}

	public void verifyDomainReg()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(domain));
			Reporter.log("domain",true);
		}
		catch(Exception e)
		{
			Reporter.log("domain is not displaying",true);
			Assert.fail();
		}
		
	}

	public void verifyverifySubmitReg()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(submit));
			Reporter.log("submit",true);
		}
		catch(Exception e)
		{
			Reporter.log("submit is not displaying",true);
			Assert.fail();
		}
		
	}

	public void entername(String Oname) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(name));
			name.sendKeys(Oname);
		}
		catch(Exception e)
		{
			Reporter.log("name is not displaying",true);
			Assert.fail();
		}
		
	}

	public void enterPassword(String string) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(password));
			password.sendKeys(string);
		}
		catch(Exception e)
		{
			Reporter.log("password is not displaying",true);
			Assert.fail();
		}
		
	}

	public void enterCnPassword(String string) {
		try{
			wait.until(ExpectedConditions.visibilityOf(confirmPassword));
			confirmPassword.sendKeys(string);
		}
		catch(Exception e)
		{
			Reporter.log("confirmPassword is not displaying",true);
			Assert.fail();
		}
		
	}

	public void enterUsername(String string) {
		try{
			wait.until(ExpectedConditions.visibilityOf(username));
			username.sendKeys(string);
		}
		catch(Exception e)
		{
			Reporter.log("username is not displaying",true);
			Assert.fail();
		}
		
	}

	public void enterOrganization(String string) {
		try{
			wait.until(ExpectedConditions.visibilityOf(organization));
			organization.sendKeys(string);
		}
		catch(Exception e)
		{
			Reporter.log("organization is not displaying",true);
			Assert.fail();
		}
		
	}

	public void clickRegDomain() {
		try{
			wait.until(ExpectedConditions.visibilityOf(submit));
			submit.click();
		}
		catch(Exception e)
		{
			Reporter.log("submit is not displaying",true);
			Assert.fail();
		}
		
	}

	public void confirmMessage() throws InterruptedException {
		Thread.sleep(3000);
		try{
			wait.until(ExpectedConditions.visibilityOf(activationMSG));
			Reporter.log(activationMSG.getText(),true);
			Thread.sleep(5000);
			Reporter.log("Get page URL",true);
			String Url = driver.getCurrentUrl();
			Reporter.log("Re-directed to : "+Url,true);			
		}
		catch(Exception e)
		{
			Reporter.log("activationMSG is not displaying",true);
			Assert.fail();
		}
		
	}

	public void verifyErrorMSG() throws InterruptedException 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(URLexistMSG));
			Reporter.log("Error message: "+URLexistMSG.getText(),true);
		}
		catch(Exception e){
			Reporter.log("URLexistMSG is not displaying",true);
			Thread.sleep(2000);
			driver.navigate().refresh();
		}
	}

	public void createDomain() throws InterruptedException 
	{
		driver.get("https://www.novaders.com/");
		WebDriverWait wait=new WebDriverWait(driver, 20);
		WebElement popup = driver.findElement(By.xpath("//a[@href='https://www.novaders.com/inout/']"));
		try{
			wait.until(ExpectedConditions.visibilityOf(popup));
			popup.click();
		}
		catch(Exception e){
			Reporter.log("Inout product ad didnt display",true);
			driver.get("https://www.novaders.com/inout");
		}		
		
		Reporter.log("\nInout application description: ",true);
		Thread.sleep(20000);
		WebElement smart = (driver.findElement(By.xpath("//h3//span[.='Smart Attendance App']")));
		wait.until(ExpectedConditions.visibilityOf(smart));
		smart.click();
		Reporter.log(smart.getText(),true);
		
		WebElement selfie = (driver.findElement(By.xpath("//h3//strong[.='Selfie Attendance']")));
		wait.until(ExpectedConditions.visibilityOf(selfie));
		selfie.click();
		Reporter.log(selfie.getText(),true);
			
		WebElement face = (driver.findElement(By.xpath("//div//h3[.='Face Detection']")));
		wait.until(ExpectedConditions.visibilityOf(face));
		face.click();
		Reporter.log(face.getText(),true);
			
		WebElement geo = (driver.findElement(By.xpath("//div//h3[.='Geo Location Tracking']")));
		wait.until(ExpectedConditions.visibilityOf(geo));
		geo.click();
		Reporter.log(geo.getText(),true);
		
		WebElement offline = (driver.findElement(By.xpath("Offline Attendance")));
		wait.until(ExpectedConditions.visibilityOf(offline));
		offline.click();
		Reporter.log(offline.getText(),true);	
		
		WebElement real = (driver.findElement(By.xpath("Real-time Analytical Reports")));
		wait.until(ExpectedConditions.visibilityOf(real));
		real.click();
		Reporter.log(real.getText(),true);
				
		WebElement notification = (driver.findElement(By.xpath("//div//h3[.='Notification']")));
		wait.until(ExpectedConditions.visibilityOf(notification));
		notification.click();
		Reporter.log(notification.getText(),true);
		
		Reporter.log("Pricing: Subscription plans are: ",true);
				
		WebElement free=driver.findElement(By.xpath("//div[1]/div/ul/li[1]/span"));
		wait.until(ExpectedConditions.visibilityOf(free));
		free.click();
		Reporter.log(free.getText(),true);
			
		WebElement business=driver.findElement(By.xpath("//div[4]/div[2]/div/ul/li[1]/span"));
		wait.until(ExpectedConditions.visibilityOf(business));
		business.click();
		Reporter.log(business.getText(),true);
		
		Reporter.log("Choose Freemium",true);
		try{
			WebElement chooseFreemium=driver.findElement(By.xpath("https://registration.inoutapps.com/#/pages/freemium"));
			chooseFreemium.click();
		}
		catch(Exception e)
		{
			driver.get("https://registration.inoutapps.com/#/pages/freemium");
		}
		
	}
}
