package pomIO;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import generic.BasePageIO;

public class DomainCreationIO extends BasePageIO
{

	public DomainCreationIO(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//*[@id='loginForm']/div[1]/div/input")
	private WebElement name;	
}