package pomIO;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import generic.BasePageIO;

public class LogsIO extends BasePageIO
{

	public LogsIO(WebDriver driver) 
	{
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//a[@href='#/logs']")
	private WebElement LogsTAB;
	
	WebDriverWait wait=new WebDriverWait(driver, 10);
	public void verifyLogs() 
	{
		wait.until(ExpectedConditions.visibilityOf(LogsTAB));
		Reporter.log(LogsTAB.getText(),true);		
	}
	
}