package pomIO;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import generic.BasePageIO;

public class RegisterIO extends BasePageIO
{

	public RegisterIO(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//img[@src='assets/img/newinout.png']")
	private WebElement inOutLogo;
	
	@FindBy(xpath="//img[@src='assets/img/appstore.png']")
	private WebElement AppStore;
	
	@FindBy(xpath="//img[@src='assets/img/play.png']")
	private WebElement Googleplay;
	
	@FindBy(xpath="/html/body/app-dashboard/ng-component/div/div/div/div/div/div[2]/div[1]/a[1]")
	private WebElement User_Login;
	
	@FindBy(xpath="//input[@placeholder='Employee Number']")
	private WebElement EmployeeNumber_USER;
	
	@FindBy(xpath="//input[@placeholder='DD']")
	private WebElement DD_USER;
	
	@FindBy(xpath="//input[@placeholder='MM']")
	private WebElement MM_USER;
	
	@FindBy(xpath="//input[@placeholder='YYYY']")
	private WebElement YYYY_User;
	
	@FindBy(xpath="//button[.='Login']")
	private WebElement Login_User;
	
	@FindBy(xpath="//button[.='Forgot password?']")
	private WebElement forgotPassword_User;
	
	@FindBy(xpath="//a[.='Sign up!']")
	private WebElement SignUp_User;
	
	@FindBy(xpath="/html/body/app-dashboard/ng-component/div/div/div/div/div/div[2]/div[1]/a[2]")
	private WebElement AdminLogin;
	
	@FindBy(xpath="//input[@placeholder='example@gmail.com']")
	private WebElement username_Admin;
	
	@FindBy(xpath="//input[@placeholder='Password']")
	private WebElement password_Admin;
	
	@FindBy(xpath="//button[.='Login']")
	private WebElement Login_Admin;
	
	@FindBy(xpath="//button[.='Forgot password?']")
	private WebElement ForgotPassword_Admin;
	
	@FindBy(xpath="//a[.='Sign up!']")
	private WebElement SignUp_Admin;
	
	WebDriverWait wait=new WebDriverWait(driver, 10);

	public void verifyInoutLogo() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(inOutLogo));
			Reporter.log("InOut Logo",true);			
		}
		catch(Exception e)
		{
			Reporter.log("InOut logo is not present",true);
			Assert.fail();
		}
		
	}

	public void verifyUserLogin() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(User_Login));
			Reporter.log("User Login",true);			
		}
		catch(Exception e)
		{
			Reporter.log("User Login is not present",true);
			Assert.fail();
		}
	}

	public void verifyAdminLogin() {
		try{
			wait.until(ExpectedConditions.visibilityOf(AdminLogin));
			Reporter.log("Admin Login",true);			
		}
		catch(Exception e)
		{
			Reporter.log("Admin Login is not present",true);
			Assert.fail();
		}
	}

	public void verifyAppStore() {
		try{
			wait.until(ExpectedConditions.visibilityOf(AppStore));
			Reporter.log("Download from the AppStore button",true);			
		}
		catch(Exception e)
		{
			Reporter.log("AppStore is not present",true);
			Assert.fail();
		}
	}

	public void verifyGooglePlay() {
		try{
			wait.until(ExpectedConditions.visibilityOf(Googleplay));
			Reporter.log("Download from the Google Play store button",true);			
		}
		catch(Exception e)
		{
			Reporter.log("Googleplay is not present",true);
			Assert.fail();
		}
	}

	public void verifyEmpnumber() {
		try{
			wait.until(ExpectedConditions.visibilityOf(EmployeeNumber_USER));
			Reporter.log(EmployeeNumber_USER.getAttribute("placeholder"),true);			
		}
		catch(Exception e)
		{
			Reporter.log("Googleplay is not present",true);
			Assert.fail();
		}
		
	}

	public void verifyDD() {
		try{
			wait.until(ExpectedConditions.visibilityOf(DD_USER));
			Reporter.log(DD_USER.getAttribute("placeholder"),true);			
		}
		catch(Exception e)
		{
			Reporter.log("DD_USER is not present",true);
			Assert.fail();
		}
		
	}

	public void verifyMM() {
		try{
			wait.until(ExpectedConditions.visibilityOf(MM_USER));
			Reporter.log(MM_USER.getAttribute("placeholder"),true);			
		}
		catch(Exception e)
		{
			Reporter.log("MM_USER is not present",true);
			Assert.fail();
		}
	}

	public void verifyYYYY() {
		try{
			wait.until(ExpectedConditions.visibilityOf(YYYY_User));
			Reporter.log(YYYY_User.getAttribute("placeholder"),true);			
		}
		catch(Exception e)
		{
			Reporter.log("YYYY_User is not present",true);
			Assert.fail();
		}
	}

	public void verifyUserLoginBTN() {
		try{
			wait.until(ExpectedConditions.visibilityOf(Login_User));
			Reporter.log(Login_User.getText(),true);			
		}
		catch(Exception e)
		{
			Reporter.log("UserLogin is not present",true);
			Assert.fail();
		}
	}

	public void verifyUserFP() {
		try{
			wait.until(ExpectedConditions.visibilityOf(forgotPassword_User));
			Reporter.log(forgotPassword_User.getText(),true);			
		}
		catch(Exception e)
		{
			Reporter.log("forgotPassword_User is not present",true);
			Assert.fail();
		}
	}

	public void clickAdminLogin() {
		try{
			wait.until(ExpectedConditions.visibilityOf(AdminLogin));	
			AdminLogin.click();
		}
		catch(Exception e)
		{
			Reporter.log("AdminLogin is not present",true);
			Assert.fail();
		}
		
	}

	public void verifyUsername() {
		try{
			wait.until(ExpectedConditions.visibilityOf(username_Admin));	
			Reporter.log("Username: "+username_Admin.getAttribute("placeholder"),true);	
		}
		catch(Exception e)
		{
			Reporter.log("username_Admin is not present",true);
			Assert.fail();
		}
		
	}

	public void verifyPassword() {
		try{
			wait.until(ExpectedConditions.visibilityOf(password_Admin));	
			Reporter.log(password_Admin.getAttribute("placeholder"),true);	
		}
		catch(Exception e)
		{
			Reporter.log("password_Admin is not present",true);
			Assert.fail();
		}
		
	}

	public void verifyAdminLoginBTN() {
		try{
			wait.until(ExpectedConditions.visibilityOf(Login_Admin));	
			Reporter.log(Login_Admin.getText(),true);	
		}
		catch(Exception e)
		{
			Reporter.log("Login_Admin is not present",true);
			Assert.fail();
		}
		
	}

	public void verifyAdminFP() {
		try{
			wait.until(ExpectedConditions.visibilityOf(ForgotPassword_Admin));	
			Reporter.log(ForgotPassword_Admin.getText(),true);	
		}
		catch(Exception e)
		{
			Reporter.log("ForgotPassword_Admin is not present",true);
			Assert.fail();
		}
		
	}

	public void verifySignUP() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(SignUp_User));
			Reporter.log(SignUp_User.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("SignUp_User is not present",true);
			Assert.fail();
		}
	}
}

//	public void verifySignUP() {
//		try{
//			wait.until(ExpectedConditions.visibilityOf(signUp));
//			Reporter.log();
//		}
//	