package pomIO;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import generic.BasePageIO;

public class LoginPageIO extends BasePageIO
{
	public LoginPageIO(WebDriver driver)
	{
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//input[@placeholder='Employee Number']")
	private WebElement employeeNumber;
	
	@FindBy(xpath="//input[@placeholder='DD']")
	private WebElement dob_DD;
	
	@FindBy(xpath="//input[@placeholder='MM']")
	private WebElement dob_MM;
	
	@FindBy(xpath="//input[@placeholder='YYYY']")
	private WebElement dob_YYYY;
	
	@FindBy(xpath="//button[.='Login']")
	private WebElement submitBTN;
	
	@FindBy(xpath="//*[@id='swal2-content']")
	private WebElement errorMSG;
	
	@FindBy(xpath="/html/body/div[2]/div/div[10]/button[1]")
	private WebElement errorOK;
	
	WebDriverWait wait=new WebDriverWait(driver, 10);
	public void setEnumber(String enumber) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(employeeNumber));
			employeeNumber.clear();
			employeeNumber.sendKeys(enumber);	
			Reporter.log("Employee number: "+enumber,true);
		}
		catch(Exception e)
		{
			Reporter.log("Employee number textbox not displayed",true);
			Assert.fail();
		}
	}

	public void setDate(String DD) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(dob_DD));
			dob_DD.clear();
			dob_DD.sendKeys(DD);
			Reporter.log("DD: "+DD,true);
		}
		catch(Exception e)
		{
			Reporter.log("DD textbox not displayed",true);
			Assert.fail();
		}
	}

	public void setMonth(String MM) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(dob_MM));
			dob_MM.clear();
			dob_MM.sendKeys(MM);
			Reporter.log("MM: "+MM,true);
		}
		catch(Exception e)
		{
			Reporter.log("MM textbox not displayed",true);
			Assert.fail();
		}
	}

	public void setYear(String YYYY)
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(dob_YYYY));
			dob_YYYY.clear();
			dob_YYYY.sendKeys(YYYY);
			Reporter.log("YYYY: "+YYYY,true);
		}
		catch(Exception e)
		{
			Reporter.log("YYYY textbox not displayed",true);
			Assert.fail();
		}
	}

	public void clickSubmit()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(submitBTN));
			submitBTN.click();	
			wait.until(ExpectedConditions.urlToBe("https://cardinal-newui.2docstore.com/#/report"));
			Reporter.log("Logged in Successfully",true);
		}
		catch (Exception e) 
		{
			String msg=errorMSG.getText();
			Reporter.log("ERROR MESSAGE: "+msg,true);
			errorOK.click();
		}
	}

	public void verifyenumber()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(employeeNumber));
			Reporter.log("Employee number textbox",true);
	}
		catch(Exception e)
		{
			Reporter.log("Employee number textbox not displayed",true);
			Assert.fail();
		}
	}

	public void verifyDD() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(dob_DD));
			Reporter.log("DD textbox",true);
	}
		catch(Exception e)
		{
			Reporter.log("DD textbox not displayed",true);
			Assert.fail();
		}
	}

	public void verifyMM() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(dob_MM));
			Reporter.log("MM textbox",true);
	}
		catch(Exception e)
		{
			Reporter.log("MM textbox not displayed",true);
			Assert.fail();
		}
	}

	public void verifyYYYY() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(dob_YYYY));
			Reporter.log("YYYY textbox",true);
	}
		catch(Exception e)
		{
			Reporter.log("YYYY textbox not displayed\n",true);
			Assert.fail();
		}
	}

	public void verifyLoginButton() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(submitBTN));
	}
		catch(Exception e)
		{
			Reporter.log("Login Button textbox not displayed",true);
			Assert.fail();
		}
	}

	public void blank() 
	{
		try{
			wait.until(ExpectedConditions.elementToBeClickable(submitBTN));
			Reporter.log("Submit button is not enabled",true);
			Reporter.log("ERROR MESSAGE: Employee number and DOB cannot be empty",true);
		}
		catch(Exception e)
		{
			Reporter.log("Submit button is not enabled",true);
			Reporter.log("ERROR MESSAGE: Employee number and DOB cannot be empty",true);
			
		}
	}
}
