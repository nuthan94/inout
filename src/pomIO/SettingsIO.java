package pomIO;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import generic.BasePageIO;

public class SettingsIO extends BasePageIO
{

	public SettingsIO(WebDriver driver) 
	{
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//a[@href='#/management']")
	private WebElement SettingsTAB;
	
	//ORGANIZATION
	@FindBy(xpath="//app-management/div[2]/div[1]/div[1]")
	private WebElement organisation_settings;
	
	@FindBy(xpath="//*[@id='input2-group2']")
	private WebElement GeofenceRadiusTB;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/form/div[2]/div/button")
	private WebElement RadiusSumitBTN;
	
	@FindBy(xpath="//div[.='Updated Successfully']")
	private WebElement RadiusUpdateMessage;
	
	@FindBy(xpath="/html/body/div[2]/div/div[10]/button[1]")
	private WebElement RadiusOK;
	
	//EXPORT
	
	@FindBy(xpath="//app-management/div[2]/div[1]/div[3]")
	private WebElement Exportdata;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/input")
	private WebElement StartDate;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/div[2]/input")
	private WebElement EndDate;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/div[3]/div[1]/div[2]")
	private WebElement Expt1;
	
	@FindBy(xpath="//*[@id='radio1']")
	private WebElement radio1;
	
	@FindBy(xpath="//*[@id='radio2']")
	private WebElement radio2;
	
	@FindBy(xpath="//*[@id='radio3']")
	private WebElement radio3;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/div[3]/div[2]/div[2]")
	private WebElement Expt2;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/div[3]/div[3]/div[2]")
	private WebElement Expt3;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/div[4]/button")
	private WebElement ExportBTN;
	
	//IMPORT
	
	@FindBy(xpath="//app-management/div[2]/div[1]/div[4]")
	private WebElement importData;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[3]/div/div[2]/button")
	private WebElement DownloadTemplate;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[3]/div/div[2]/input")
	private WebElement ChooseFile;
	
	//PREFERENCE
	@FindBy(xpath="//app-management/div[2]/div[1]/div[2]")
	private WebElement preference;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div/div[2]/div[1]/div")
	private WebElement P_Department_Label;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div/div[2]/div[2]/div")
	private WebElement P_Location_Label;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div/div[2]/div[3]/div")
	private WebElement P_DateRange_Label;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div/div[2]/div[1]/angular2-multiselect/div/div[1]/div")
	private WebElement P_Department;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div/div[2]/div[1]/angular2-multiselect/div/div[2]/div[2]/ul/li[1]")
	private WebElement P_Department_Market;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div/div[2]/div[1]/angular2-multiselect/div/div[2]/div[2]/ul/li[2]")
	private WebElement P_Department_Testing;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[1]")
	private WebElement PWKL_JAKARTA_KONSI_DAN_PUTUS;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[2]")
	private WebElement PUSAT_KRAWANG;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[3]")
	private WebElement PWKL_JATIM_KONSINYASI;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[4]")
	private WebElement SPG_JAKARTA;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[5]")
	private WebElement SPG_JAWA_TENGAH;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[6]")
	private WebElement SPG_JAWA_TIMUR;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[7]")
	private WebElement PWK_JATENG_KONSINYASI;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[8]")
	private WebElement SPG_JAWA_BARAT;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[9]")
	private WebElement SPG_GUDANG_GARMENT;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[2]/angular2-multiselect/div/div[2]/div[2]/ul/li[10]")
	private WebElement SPG_SHOWROOM;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div/div[2]/div[2]/angular2-multiselect/div/div[1]/div")
	private WebElement P_Location;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div/div[2]/div[3]/angular2-multiselect/div/div[1]/div")
	private WebElement P_DateRange;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[3]/angular2-multiselect/div/div[2]/div[2]/ul/li[1]")
	private WebElement Today;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[3]/angular2-multiselect/div/div[2]/div[2]/ul/li[2]")
	private WebElement Yesterday;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[3]/angular2-multiselect/div/div[2]/div[2]/ul/li[3]")
	private WebElement Last7Days;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[3]/angular2-multiselect/div/div[2]/div[2]/ul/li[4]")
	private WebElement Last30Days;
	
	@FindBy(xpath="//app-management/div[2]/div[2]/div/div/div[2]/div[4]/div/button")
	private WebElement preferenceSubmit;
	
	@FindBy(xpath="/html/body/app-dashboard/header/ul[2]/li[3]/a/img")
	private WebElement logout;
	
	@FindBy(xpath="/html/body/app-dashboard/header/ul[2]/li[3]/div/a")
	private WebElement userLogout;
	
	WebDriverWait wait=new WebDriverWait(driver, 10);

	public void verifySettings() 
	{
		wait.until(ExpectedConditions.visibilityOf(SettingsTAB));
		Reporter.log(SettingsTAB.getText(),true);
	}

	public void clickSettings() {
		wait.until(ExpectedConditions.visibilityOf(SettingsTAB));
		SettingsTAB.click();		
	}

	public void clickOrganisationSettings() {
		wait.until(ExpectedConditions.visibilityOf(organisation_settings));
		organisation_settings.click();
		
	}

	public void radiusBlank() throws InterruptedException {
		Thread.sleep(4000);
		GeofenceRadiusTB.clear();
		try{
			RadiusSumitBTN.click();
		}
		catch(Exception e)
		{
			Reporter.log("Radius submit is not enabled",true);
		}
	}

	public void verifyOrganisationSettings() {
		wait.until(ExpectedConditions.visibilityOf(organisation_settings));
		Reporter.log(organisation_settings.getText(),true);		
	}

	public void verifyPreferences() {
		wait.until(ExpectedConditions.visibilityOf(preference));
		Reporter.log(preference.getText(),true);
	}

	public void verifyExport() {
		wait.until(ExpectedConditions.visibilityOf(Exportdata));
		Reporter.log(Exportdata.getText(),true);
		
	}

	public void verifyImport() {
		wait.until(ExpectedConditions.visibilityOf(importData));
		Reporter.log(importData.getText(),true);
	}

	public void setRadius(String Radius) 
	{
		GeofenceRadiusTB.clear();
		Reporter.log("Entered value:"+Radius,true);
		GeofenceRadiusTB.sendKeys(Radius);
		try{
			RadiusSumitBTN.click();
			Thread.sleep(3000);
			Reporter.log("MESSAGE: "+RadiusUpdateMessage.getText(),true);
			RadiusOK.click();
		}
		catch(Exception e)
		{
			Reporter.log("Radius submit is not enabled",true);
			Reporter.log("ERROR: Radius textbox doesnot accept alphabets/ special characters",true);
		}
	}

	public void clickLogout() {
		wait.until(ExpectedConditions.visibilityOf(logout));
		logout.click();
	}

	public void clickUserlogout(String enumber) {
		wait.until(ExpectedConditions.visibilityOf(userLogout));
		userLogout.click();
		Reporter.log("Logged out as"+enumber,true);
	}

	public void verifyRadius() {
		wait.until(ExpectedConditions.visibilityOf(GeofenceRadiusTB));
		try{
			Assert.assertEquals(GeofenceRadiusTB.getText(), "2000");
			Reporter.log("Actual radius value: "+GeofenceRadiusTB.getText()+" Expected radius value: 2000");
		}
		catch(Exception e){
			Reporter.log("Radius value are not matching",true);
		}
		
		
	}
}