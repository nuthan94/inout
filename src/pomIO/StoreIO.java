package pomIO;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import generic.BasePageIO;

public class StoreIO extends BasePageIO
{

	public StoreIO(WebDriver driver)
	{
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//button[.='Store']")
	private WebElement storeTAB;	
	
	@FindBy(xpath="//button[@touranchor='master.addEmployee']")
	private WebElement addStore;
	
	@FindBy(xpath="//button[@touranchor='master.export']")
	private WebElement storeExport;
	
	@FindBy(xpath="//*[@id='storenum1']")
	private WebElement addStoreNum;
	
	@FindBy(xpath="//*[@id='store1']")
	private WebElement addStoreName;
	
	@FindBy(xpath="//*[@id='address1']")
	private WebElement addStoreAddress;
	
	@FindBy(xpath="//*[@id='radius']")
	private WebElement addStoreRadius;
	
	@FindBy(xpath="//*[@id='lat']")
	private WebElement addStoreLatitude;
	
	@FindBy(xpath="//*[@id='long']")
	private WebElement addStoreLongitude;
	
	@FindBy(xpath="//button[.='Cancel']")
	private WebElement addStoreCancel;
	
	@FindBy(xpath="//button[.='Submit']")
	private WebElement addStoreSubmit;
	
	WebDriverWait wait=new WebDriverWait(driver, 20);

	public void clickStoreTab() 
	{
		wait.until(ExpectedConditions.visibilityOf(storeTAB));
		storeTAB.click();
	}

	public void clickAddStore() 
	{
		wait.until(ExpectedConditions.visibilityOf(addStore));
		addStore.click();
	}

	public void enterStoreNumber(String storeNumber) 
	{
		wait.until(ExpectedConditions.visibilityOf(addStoreNum));
		addStoreNum.sendKeys(storeNumber);
	}

	public void enterStoreName(String storeName) 
	{
		wait.until(ExpectedConditions.visibilityOf(addStoreName));
		addStoreName.sendKeys(storeName);		
	}

	public void enterStoreAddress(String storeAddress) 
	{
		wait.until(ExpectedConditions.visibilityOf(addStoreAddress));
		addStoreAddress.sendKeys(storeAddress);
	}

	public void enterStoreRadius(String storeRadius) 
	{
		wait.until(ExpectedConditions.visibilityOf(addStoreRadius));
		addStoreRadius.sendKeys(storeRadius);
	}

	public void enterStoreLatitude(String storeLatitude) 
	{
		wait.until(ExpectedConditions.visibilityOf(addStoreLatitude));
		addStoreLatitude.sendKeys(storeLatitude);		
	}

	public void enterStoreLongitude(String storeLongitude) 
	{
		wait.until(ExpectedConditions.visibilityOf(addStoreLongitude));
		addStoreLongitude.sendKeys(storeLongitude);
	}

	public void clickStoreSubmit() 
	{
		wait.until(ExpectedConditions.visibilityOf(addStoreSubmit));
		addStoreSubmit.click();
	}

}
